$(document).ready(function(){
    $(this).scroll(function(){
        $(".navUserNameText").animate({opacity : '1'});
        $(".img").animate({opacity : '1'});
        $(".imgExt").animate({opacity : '1'});
        $(".contentPhotosLeft").animate({opacity : '1'});
        $(".scrollDownText").animate({opacity : '0.2'});
        $(".icon-down-dir").animate({fontSize : '80px'});
    });
});